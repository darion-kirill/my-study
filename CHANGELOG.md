## 1.0.3 (2021-11-09)

### feature (2 changes)

- [feature(#1) New feature](darion-kirill/my-study@161ca08549209054f60a8f2fbf05266d4217ff32)
- [feature(#1) New feature](darion-kirill/my-study@267ba174b95f73965c874dd5b15b64f8d623da8b)

## 1.0.2 (2021-11-09)

### feature (2 changes)

- [feature(#1) New feature](darion-kirill/my-study@ebe52a4a7e8815966d0c04b52ecd9ec23139c38a)
- [feature(#1) New feature](darion-kirill/my-study@aa38e95283a5f16b87fb233238a349d7ec50915a)

## 1.0.1 (2021-11-09)

### feature (7 changes)

- [feature(#1) New feature](darion-kirill/my-study@8335dbab8adc741b3f03ec0f7b8b62f03d68a8e3)
- [bug(#1) Bugfix](darion-kirill/my-study@5c51de03426cef58f021fdaf3a794d07b54a9b72)
- [bug(#1) Bugfix](darion-kirill/my-study@5db5613463dd1cdb87741350ee3076aa383e455d)
- [feature(#1) Fixed bug](darion-kirill/my-study@1292faf5642e48d47dff9156bd070d8ddb28b4fc)
- [feature(#1) Fixed bug](darion-kirill/my-study@26cdc5f9e2461bba76e86e2ceb0e00e62b98a537)
- [feature(#1) Fixed bug](darion-kirill/my-study@c8f2397e62c3816ba3fcd66d827fbbf128b9722d)
- [feature(#1) Fixed bug](darion-kirill/my-study@5d451a47bae5e79a5ae5a29a3ef9ac079b89c8b9)

### bug (2 changes)

- [bug(#1) Bugfix](darion-kirill/my-study@28a00f340ba54cd7eb266200c358cc89f97ec4e0)
- [bug(#1) Bugfix](darion-kirill/my-study@5d3941ad8ed1d4ecee514ce8a1fac46e2c4609b1)
