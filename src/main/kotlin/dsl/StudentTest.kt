package dsl

data class Student(val name: String, val course: String, val grades: List<Double>)

fun student(block: StudentBuilder.() -> Unit): Student = StudentBuilder().apply(block).build()

class StudentBuilder{
    var name = ""
    var course = ""
    var grades = listOf<Double>()

    fun build(): Student = Student(name, course, grades)
}

fun main() {

    val studentNew = student {
        name = "Max"
        course = "Kotlin "
        grades = listOf(4.24, 5.0, 3.2)
    }

    println(studentNew)
}