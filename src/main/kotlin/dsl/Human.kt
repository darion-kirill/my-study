package dsl

class Human(var name: String = "", var age: Int = 0, var addresses: MutableList<Address> = mutableListOf()){
    override fun toString(): String {
        return "Human $name $age $addresses "
    }
}

data class Address(var street: String = "", var number: Int = 0, var city: String = "")

fun human(block: Human.() -> Unit): Human = Human().apply(block)
fun Human.address(block: Address.() -> Unit){
    addresses.add(Address().apply(block))
}

fun main() {

    val max = human {
        name = "Max"
        age = 10
        address {
            street = "Tverskaya"
            number = 30
            city = "Moscow"
        }
        address {
            street = "Main street"
            number = 23
            city = "Texas"
        }
    }

    println(max)

}